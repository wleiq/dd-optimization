#ifndef DDCOSTFUNCTION_DD_H
#define DDCOSTFUNCTION_DD_H

#include "Clique.h"

namespace DD
{
	class Clique;
	/**
	 * \brief (Abstract class) DDCostFunction class. It models a generic cost function used by Dual Decomposition to evaluate a clique.
	 * 
	 * \author Enzo Ferrante.
	 * \date 28-11-2012
	 *
	 */
	class DDCostFunction
	{
		public:
			DDCostFunction(float factor = 1.0f) : factor(factor) {};

			/**
			* Evaluate the cost function in the given clique using the given labels, and return its cost.
			* \param clique the clique that will be evaluated
			* \param labelAssignement an array containing the label assignment for each node in the graph. Some nodes can
			*		have an INVALID_LABEL, but the ones involved in the clique must have a valid value.
			*/
			virtual float evaluate(Clique * clique, int * labelAssignment) = 0;

			void setFactor(float factor) { this->factor = factor; }
			float getFactor() { return factor; }

		protected:

			float factor;
	};
};

#endif