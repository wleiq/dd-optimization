#include "DDGraph.h"

namespace DD
{

    DDGraph::DDGraph(int rows, int cols, int dimLabel) : numRows(rows), numCols(cols), nodesCount(rows * cols), dimLabel(dimLabel)
	{
		nodesSlaves = new QVarLengthArray<Slave *, MAX_SLAVES>[nodesCount]; 

	}

	void DDGraph::eraseSlavesInformation() 
	{
		for (int i = 0; i < nodesCount; ++i)
		{
			nodesSlaves[i].clear();
		}
	}

	DDGraph::~DDGraph()
	{
		delete nodesSlaves;
	}

}
