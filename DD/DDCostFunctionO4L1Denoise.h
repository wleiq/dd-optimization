#ifndef DDCOSTFUNCTIONO4L1DENOISE_H
#define DDCOSTFUNCTIONO4L1DENOISE_H

#include "DDCostFunction.h"
#include "LabelTableSimple.h"
#include "DualDecomposition.h"

namespace DD {

    /**
     * \brief A label table that contains 1D labels.
     *
     * \author Enzo Ferrante - Hariprasad Kannan
     *
     */

    class DDCostFunctionO4L1Denoise : public DDCostFunction
    {

    public:
        DDCostFunctionO4L1Denoise(LabelTableSimple * labelTable, float *ipImg, int dimX, int dimY, float factor = 1.0);

        float evaluate(Clique * clique, int * labelAssignment);

        void precalculateCosts(DualDecomposition *dd);

    private:
        LabelTableSimple * m_labelTable;
        float * m_costs;
        int m_l;
        int m_l_quadruple;
        int m_l_cube;
        int m_l_squared;
        int m_numCliques;
        float* m_ipImg;
        int m_dimX;
        int m_dimY;
    };
}

#endif // DDCOSTFUNCTIONO4L1DENOISE_H
