#ifndef LABELTABLESIMPLE_H
#define LABELTABLESIMPLE_H

#include "LabelTable.h"

namespace DD{
    /**
     * \brief A simple table that returns one dimensional labels, where the value of the label is the index itself.
     *
     * \author Enzo Ferrante - Hariprasad Kannan
     * \date 3-4-2015
     *
     */
class LabelTableSimple : public LabelTable
{
public:

    LabelTableSimple(int numberOfLabels) : LabelTable(1, numberOfLabels) {}

    /**
    * Returns the label corresponding to the given index (in this case, it is a 1-dimensional label where the value is the labelIndex itself.
    */
    const float * getLabel(int labelIndex)
    {
        float * result = new float[1];
        result[0] = labelIndex;
        return result;
    }
};

}
#endif // LABELTABLESIMPLE_H
